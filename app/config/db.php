<?php

class db
{
    // Se definen las variables de conexion para la base de datos protegidas en este contexto
    private $dbhost = 'localhost';
    private $dbuser = 'root';
    private $dbpass = '';
    private $dbname = 'social_network';

    /**
     * @return PDO
     *
     *  esta funcion abre una conexion a la base de datos especificada en las variables de conexion
     *  utilizando clases abstractas capturando sus propias exepciones y codificando el las respuestas
     *  en UTF-8 para que concuerden con el idioma.
     *
     */
    public function connect()
    {
        $mysql_connect_str = "mysql:host=$this->dbhost;dbname=$this->dbname";
        $dbConnection = new PDO($mysql_connect_str, $this->dbuser, $this->dbpass);
        $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbConnection->exec("set names utf8");
        return $dbConnection;
    }
}