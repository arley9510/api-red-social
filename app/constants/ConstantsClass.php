<?php

/**
 * Class ConstantsClass
 *
 *  en esta clase se definen las constantes a utilizar en las respuestas de los servicios
 */
class ConstantsClass
{
    const CODE_SUCCESS_REGISTER = '{"OK": "Usuario registrado con exito"}';
    const CODE_ERROR_REGISTER_EMAIL = '{"error": ""}';
    const CODE_ERROR_REGISTER_DATA = '{"error": ""}';
}