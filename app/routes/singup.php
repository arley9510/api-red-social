<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once('user/SingUp.php');


// se instancian los objetos a utilizar en el servicio
$message = new ConstantsClass();
$singUp = new SingUp();
$log = new LogClass();

/**
 *  en este metodo post se realiza el registro de los usuarios en la plataforma.
 *
 *  con los parametros de entrada var @name, var @email, y var @password.
 *
 *  se valida que el email ingresado no este registrado con anterioridad
 *  de lo contrario no se realiza el registro.
 *
 *  en caso de un registro exitoso se retorna un status code 201
 *  en el caso que el registro falle se retorna un status code 422
 */
$app->post('/api/sing-up', function (Request $request, Response $response) use ($singUp, $log, $message) {

    $name = $request->getParam('name');
    $email = $request->getParam('email');
    $password = $request->getParam('password');

    try {

        $newUser = $singUp->singUp($name, strtolower($email), $password);

        if ($newUser === true) {

            echo $message::CODE_SUCCESS_REGISTER;

            return $response->withStatus(201);

        } elseif ($newUser === 'email_error') {
            echo $message::CODE_ERROR_REGISTER_EMAIL;

            return $response->withStatus(422);
        } else {
            echo $message::CODE_ERROR_REGISTER_DATA;

            return $response->withStatus(422);
        }
    } catch (PDOException $PDOException) {

        echo '{"error": ' . $PDOException->getMessage() . '}';
        $log->sendLog($PDOException);

        return $response->withStatus(500);
    }
});