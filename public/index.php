<?php

require_once('../vendor/sentry/sentry/lib/Raven/Autoloader.php');
require_once '../app/constants/ConstantsClass.php';
require_once '../app/log/LogClass.php';
require_once '../vendor/autoload.php';
require_once '../app/config/db.php';

Raven_Autoloader::register();

$app = new \Slim\App;
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

[
    require_once '../app/routes/singup.php'
];

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', '*');
});

try {
    $app->run();
} catch (\Slim\Exception\MethodNotAllowedException $e) {
    $log->sendLog($e);
} catch (\Slim\Exception\NotFoundException $e) {
    $log->sendLog($e);
} catch (Exception $e) {
    $log->sendLog($e);
}