<?php

class LogClass
{
    private $client;

    /**
     * LogClass constructor.
     *
     *  se inicializa en esta clase el servidor al cual se hara la el envio de las exepciones del app
     */
    public function __construct()
    {
        try {
            $this->client = (new Raven_Client('https://bd583fb0181c4ded87b22ec5449ff452:cfbd18ba92ff41e49a6f4daf61549822@sentry.io/1199165'))->install();
        } catch (Raven_Exception $e) {
            echo $e;
        }
    }

    /**
     * @param $event
     *
     *  en esta funcion se envian al servidor se sentry las exepciones para su respectivo monitoreo
     *
     */
    public function sendLog($event)
    {

        $this->client->captureException($event);

    }

}