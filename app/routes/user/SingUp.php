<?php

class SingUp
{
    private $log;

    public function __construct()
    {
        $this->log = new LogClass();
    }


    /**
     * @param $name
     * @param $email
     * @param $password
     * @return string
     *
     *  en esta funcion se valida var @email
     *  para validar si el usuario ya esta registrado de lo contrario registra al usuario
     *
     */
    public function singUp($name, $email, $password)
    {
        $isValid = $this->validateEmail($email);

        if ($isValid === null) {

            return $this->newUser($name, $email, $password);
        } else {

            return 'email_error';
        }

    }

    /**
     * @param $email
     * @return int
     *
     *  en eta funcion se balida el email del usuario que se intenta registar
     *  retorna el id del usuario con el correo de var @emial si este esta registrado de lo contarrio
     *  retorna null.
     *
     */
    public function validateEmail($email)
    {
        $sql = "SELECT person_person_id FROM user WHERE email = '$email'";
        $id = null;

        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql);

            while (($row = $stmt->fetch(PDO::FETCH_ASSOC)) !== false) {
                $id = $row;
            }
            $db = null;

            return intval($id);

        } catch (PDOException $PDOException) {

            $this->log->sendLog($PDOException);
        }
    }

    /**
     * @param $name
     * @param $email
     * @param $password
     * @return bool
     *
     *  en esta funcion se realiza el registro del usuario se resiven los parametos
     *  var @name, var @email y var @password para realizar el registro en el sistema
     *
     *  se deben realizar dos inserciones en las base de datos una pra la entidad de persona
     *  y otra para la entidad de usuario pra que este pueda realizar el login dentro de la plataforma
     *
     */
    public function newUser($name, $email, $password)
    {
        $sql = "INSERT INTO person VALUES(:name)";
        $id  = "SELECT person_id FROM person WHERE name = '$name'";
        $insertUser = "INSERT INTO user VALUES(:person_person_id, :email, :password)";

        try {

            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql);
            $stmt->execute();

            $stmt2 = $db->query($id);
            $stmt2->execute();

            $stmt3 = $db->query($insertUser);
            $stmt3->bindParam('person_person_id', $id);
            $stmt3->bindParam('email', $email);
            $stmt3->bindParam('password', $password);

            return $stmt3->execute();


        } catch (PDOException $PDOException) {

            $this->log->sendLog($PDOException);
        }
    }


}